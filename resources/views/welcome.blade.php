@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row  col-md-offset-2 custyle">
        <table class="table table-striped custab" id="myTable">
            <thead>
                <button type="button" class="btn btn-info btn-lg pull-right" data-toggle="modal" data-target="#myModal">Add new</button>
                
                <tr>
                    <th>ID</th>
                    <th>SapId</th>
                    <th>HostName</th>
                    <th>LoopBack</th>
                    <th>Mac Address</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            @if(!$routerData->isEmpty())
                    @foreach($routerData as $key => $router)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$router->sap_id}}</td>
                    <td>{{$router->host_name}}</td>
                    <td>{{$router->loop_back}}</td>
                    <td>{{$router->mac_address}}</td>
                    <td class="text-center"><a class='btn btn-info btn-xs' href="#"><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="#" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
                </tr>
                @endforeach
            @endif
        </table>
    </div>
</div>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Store Data</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="form">
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                          <label for="validationServer013">Sap Id</label>
                          <input type="text" class="form-control is-valid" id="sap_id" name="sap_id" placeholder="Sap Id" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label for="validationServer023">Host name</label>
                          <input type="text" class="form-control is-valid" id="host_name" placeholder="Host Name" name="host_name" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label for="validationServer023">Loop Back</label>
                          <input type="text" class="form-control is-valid" id="loop_back" placeholder="Loop Back" name="loop_back" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label for="validationServer023">Mac Address</label>
                          <input type="text" class="form-control is-valid" id="mac_address" placeholder="Mac Address" name="mac_address" required>
                        </div>
                    </div>
                </form>
            </div>
        </div>
            <div class="modal-footer">
              <button type="button" id="submit" class="btn btn-default">Submit</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@push('js_scripts')
<script type="text/javascript">
$(document).ready(function(){
    $(document).ready( function () {
        $('#myTable').DataTable();
    });

    
    $("#submit").click(function(){
       // $("button").trigger("click");
        if($("#sap_id").val() !== '' && $("#host_name").val() !== '' &&
            $("#loop_back").val() !== '' && $("#mac_address").val() !== ''){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url:"{{route('storeRouter')}}",
                type:"POST",
                data: $('#form').serialize(),
                beforeSend:function(){
                    alert(this.data);
                },   
                success:function(data)
                {
                    console.log(data);
                    // $("#permissions-append").html(data);
                },
                error: function(e)
                {
                    // location.reload();
                }
            });
        }else{
            alert('All fields are required!');
        }
    });
});
</script>
@endpush
@endsection