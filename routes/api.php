<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {
Route::post('/login','UserController@login')->name('login');
	Route::group(['middleware' => 'auth:api'], function(){
		Route::post('/create','RouterDetailController@createRouter')->name('create');
		Route::post('/update','RouterDetailController@updateRouter')->name('update');
		Route::post('/delete','RouterDetailController@deleteRouter')->name('delete');
	});
});
