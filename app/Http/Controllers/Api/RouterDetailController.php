<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Repository\RouterRepository;
use Response;
use DB;
use Log;

class RouterDetailController extends Controller
{
    private $routerRepository;
    public function __construct(RouterRepository $routerRepository)
    {
        $this->routerRepository = $routerRepository;
    }

    public function createRouter(request $request){
        try{
            $validator=Validator::make($request->all(),[
                "sap_id"            => 'required|unique:router_detail',
                "loop_back"         => "required|unique:router_detail",
                "host_name"         => "required|unique:router_detail",
                "mac_address"       => "required|unique:router_detail",
            ]);

            if($validator->fails()){
                return response()->json(['success'=>false, 'message'=>$validator->errors()->first()], 200);   
            }else{
                $checkIfExist = $this->routerRepository->checkIfExist($request->all());
                if(empty($checkIfExist)){
                    $createdRouter = $this->routerRepository->createRouter($request->all());
                    if(!empty($createdRouter)){
                        return response()->json(array('success'=>true, 'message'=>"Router Sucessfully created.",'data'=>array()), 200);
                    }else{
                        return response()->json(array('success'=>false, 'message'=>"Please try  later",'data'=>array()), 200);
                    }
                }else{
                    return response()->json(array('success'=>false, 'message'=>"all fields should be unique",'data'=>array()), 200);
                }
            }
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(array('success'=>false, 'message'=>"Something wrong, Please try later",'data'=>array()), 200);
        }
    }

    public function updateRouter(request $request){
        try{
            $validator=Validator::make($request->all(),[
                "sap_id"            => 'required'
            ]);

            if($validator->fails()){
                return response()->json(['success'=>false, 'message'=>$validator->errors()->first()], 200);   
            }else{
                $checkIfExist = $this->routerRepository->checkIfSapIdExist($request->all());
                if(!empty($checkIfExist)){
                    $createdRouter = $this->routerRepository->updateRouterData($request->all());
                    if(!empty($createdRouter)){
                        return response()->json(array('success'=>true, 'message'=>"Details Sucessfully updated.",'data'=>array()), 200);
                    }else{
                        return response()->json(array('success'=>false, 'message'=>"Please try  later",'data'=>array()), 200);
                    }
                }else{
                    return response()->json(array('success'=>false, 'message'=>"Sap Id not found",'data'=>array()), 200);
                }
            }
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(array('success'=>false, 'message'=>"Something wrong, Please try later",'data'=>array()), 200);
        }
    }

    public function deleteRouter(request $request){
        try{
            $validator=Validator::make($request->all(),[
                "sap_id"    => 'required'
            ]);

            if($validator->fails()){
                return response()->json(['success'=>false, 'message'=>$validator->errors()->first()], 200);   
            }else{
                $checkIfExist = $this->routerRepository->checkIfSapIdExist($request->all());
                if(empty($checkIfExist)){
                    $request['permanently_delete'] = 1;
                    $createdRouter = $this->routerRepository->deleteRouterData($request->all());
                    if(!empty($createdRouter)){
                        return response()->json(array('success'=>true, 'message'=>"Router Sucessfully deleted.",'data'=>array()), 200);
                    }else{
                        return response()->json(array('success'=>false, 'message'=>"Please try  later",'data'=>array()), 200);
                    }
                }else{
                    return response()->json(array('success'=>false, 'message'=>"all fields should be unique",'data'=>array()), 200);
                }
            }
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(array('success'=>false, 'message'=>"Something wrong, Please try later",'data'=>array()), 200);
        }
    }
}