<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Password;
use Lcobucci\JWT\Parser;
use App\Models\User;
use Response;
use DB;
use Log;


class UserController extends Controller
{
    public function __construct()
    {

    }

    /*User Login Function*/
    public function login(Request $request){
        try {
            $validator=Validator::make($request->all(),[
                "email"            => 'required',
                "password"         => "required",
            ]);

            if($validator->fails()){
                return response()->json(['success'=>false, 'message'=>$validator->errors()->first()], 200);   
            }else{
            
                \Auth::attempt(['email' => $request->email, 'password' => $request->password]);
                $user = \Auth::user();
                if(!empty($user)){
                    $user['token'] = 'Bearer '.$user->createToken(request()->json()->get('email'))->accessToken;
                    return response()->json(array('success'=>true, 'message'=>"Logged in successfull",'data'=>$user), 200);
                }else{
                    return response()->json(array('success'=>false, 'message'=>"invalid user credentials",'data'=>[]), 200);
                }
            }
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(array('success'=>false, 'message'=>"Something wrong, Please try later",'data'=>array()), 200);
        }
    }
}