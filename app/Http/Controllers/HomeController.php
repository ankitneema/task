<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\RouterRepository;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $routerRepository;
    public function __construct(RouterRepository $routerRepository)
    {
        $this->routerRepository = $routerRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $routerData = $this->routerRepository->getAllRouter();
        return view('welcome',['routerData'=>$routerData]);
    }

    public function storeRouter(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                "sap_id"            => 'required|unique:router_detail',
                "loop_back"         => "required|unique:router_detail",
                "host_name"         => "required|unique:router_detail",
                "mac_address"       => "required|unique:router_detail",
            ]);
            if($validator->fails()){
                return response()->json(['success'=>false, 'message'=>$validator->errors()->first()], 200);   
            }else{
                $checkIfExist = $this->routerRepository->checkIfExist($request->all());
                if(empty($checkIfExist)){
                    $createdRouter = $this->routerRepository->createRouter($request->all());
                    if(!empty($createdRouter)){
                        return response()->json(array('success'=>true, 'message'=>"Router Sucessfully created.",'data'=>array()), 200);
                    }else{
                        return response()->json(array('success'=>false, 'message'=>"Please try  later",'data'=>array()), 200);
                    }
                }else{
                    return response()->json(array('success'=>false, 'message'=>"all fields should be unique",'data'=>array()), 200);
                }
            }
        }catch (\Exception $e) {
            Log::error($e);
            return response()->json(array('success'=>false, 'message'=>"Something wrong, Please try later",'data'=>array()), 200);
        }
    }
}