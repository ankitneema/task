<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class RouterDetail extends Model
{
    protected  $table     = "router_detail";
    protected  $guarded   = ['id'];
}