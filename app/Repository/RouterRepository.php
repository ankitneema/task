<?php
namespace App\Repository;

use App\RouterDetail;
class RouterRepository
{
    private $routerDetail;
    public function __construct(RouterDetail $routerDetail)
    {
        $this->routerDetail = $routerDetail;
    }

    public function checkIfExist($params = array())
    {
        try {
            $routerDetail = array();
            if(!empty($params)){

                $routerDetail = RouterDetail::select('id')->where('sap_id',$params['sap_id'])
                    ->orWhere('loop_back',$params['loop_back'])
                    ->orWhere('host_name',$params['host_name'])
                    ->orWhere('mac_address',$params['mac_address'])->first();
            }
            return $routerDetail;
        } catch (\Exception $e) {
            \Log::error($e);
            throw new \Exception("Unable to get router");
        }
    }

    public function createRouter($params = array())
    {
        try {
            $routerCreate = array();
            if(!empty($params)){

                $routerCreate = RouterDetail::create([
                    'sap_id'=>$params['sap_id'],
                    'loop_back'=>$params['loop_back'],
                    'host_name'=>$params['host_name'],
                    'mac_address'=>$params['mac_address'],
                    'status'=>1
                ]);
            }
            return $routerCreate;
        } catch (\Exception $e) {
            \Log::error($e);
            throw new \Exception("Unable to get router");
        }
    }

    public function checkIfSapIdExist($params = array())
    {
        try {
            $routerDetail = array();
            if(!empty($params)){
                $routerDetail = RouterDetail::select('id')->where('sap_id',$params['sap_id'])
                    ->first();
            }
            return $routerDetail;
        } catch (\Exception $e) {
            \Log::error($e);
            throw new \Exception("Unable to get router");
        }
    }

    public function updateRouterData($params = array())
    {
        try {
            $routerCreate = array();
            if(!empty($params)){

                $routerCreate = RouterDetail::where('sap_id',$params['sap_id'])->update([
                    'loop_back'=>$params['loop_back'],
                    'host_name'=>$params['host_name'],
                    'mac_address'=>$params['mac_address']
                ]);
            }
            return $routerCreate;
        } catch (\Exception $e) {
            \Log::error($e);
            throw new \Exception("Unable to router data");
        }
    }

    public function deleteRouterData($params = array())
    {
        try {
            $routerDeleted = array();
            if(!empty($params)){
                $routerDeleted = RouterDetail::where('sap_id',$params['sap_id'])->delete();
            }
            return $routerDeleted;
        } catch (\Exception $e) {
            \Log::error($e);
            throw new \Exception("Unable to Delete data");
        }
    }

    public function getAllRouter(){
        try {
                return RouterDetail::where('status',1)->orderby('id','desc')->get();
        } catch (\Exception $e) {
            \Log::error($e);
            throw new \Exception("Unable to Delete data");
        }
    }
}